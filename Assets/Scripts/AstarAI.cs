using UnityEngine;
using System.Collections;
//Note this line, if it is left out, the script won't know that the class 'Path' exists and it will throw compiler errors
//This line should always be present at the top of scripts which use pathfinding
using Pathfinding;

public class AstarAI : MonoBehaviour {
	public float randMin, randMax;
	public Path path; 			//The calculated path
	public float speed = 2; 	//The AI's speed per second

	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;

	private int currentWaypoint = 0; //The waypoint we are currently moving towards
	private int areaIndex;
	private float timer;

	private int[] areaIndecesCopy;
	private int numDestroyed = 0;

	private Transform target;	//The point to move to
	private Seeker seeker;
	private AreaManagerScript areaScript;

	public void Start() {
		GameObject areaManager = GameObject.Find("Areas");
		areaScript = areaManager.GetComponent<AreaManagerScript>();
		areaIndecesCopy = new int[areaScript.areas.Length];
		for (int i = 0; i < areaScript.areas.Length; i++) {
			areaIndecesCopy[i] = i;
		}
		if (numDestroyed < areaScript.areas.Length) {
			areaIndex = PickValidAreaIndex(areaScript.areas);

			//Sets the entrance of an area as the destination for the NPC
			target = areaScript.areas[areaIndex].transform.Find("Entrance").GetComponent<Transform>();
			timer = Random.Range(randMin, randMax);

			//Start a new path to the targetPosition, return the result to the OnPathComplete function
			seeker = GetComponent<Seeker>();
			seeker.StartPath (transform.position, target.position, OnPathComplete);
		}
	}

	public void OnPathComplete(Path p) {
    	if (!p.error) {
    		path = p;
    		//Reset the waypoint counter
    		currentWaypoint = 0;
    	}
	}

  	public void FixedUpdate () {
		if (path != null) {
			if (currentWaypoint >= path.vectorPath.Count) {
				timer -= Time.deltaTime;
				if (timer <= 0) {
					timer = Random.Range(randMin, randMax);
					if (numDestroyed < areaScript.areas.Length) {
						areaIndex = PickValidAreaIndex(areaScript.areas);
						target = areaScript.areas[areaIndex].transform.Find("Entrance").GetComponent<Transform>();
						path = seeker.StartPath(transform.position, target.position, OnPathComplete);
					}
				}
				return;
			}

			//Direction to the next waypoint
			Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
			dir *= speed * Time.fixedDeltaTime;
			this.gameObject.transform.Translate(dir);
		
			//Check if we are close enough to the next waypoint
			//If we are, proceed to follow the next waypoint
			if (Vector3.Distance (transform.position, path.vectorPath[currentWaypoint]) <
			 nextWaypointDistance) {
				currentWaypoint++;
			}
		}
	}

	int PickValidAreaIndex(GameObject[] areaArray) {
		bool needToUpdate = false;
		int temp = 0;

		areaIndex = areaIndecesCopy[Random.Range(0, areaArray.Length - numDestroyed)];
		if (!areaArray[areaIndex].activeSelf) {
			for (int i = 0; i < areaArray.Length && !needToUpdate; i++) {
				if (areaIndecesCopy[i] == areaIndex) {
					needToUpdate = true;
				}
			}
			if (needToUpdate) {
				numDestroyed++;
				Debug.Log ("Number of areas destroyed: " + numDestroyed);
				for (int i = 0; i < areaIndecesCopy.Length; i++) {
					if (areaIndecesCopy[i] == areaIndex) {
						while (i < areaIndecesCopy.Length - 1) {
							temp = areaIndecesCopy[i];
							areaIndecesCopy[i] = areaIndecesCopy[i + 1];
							areaIndecesCopy[i + 1] = temp;
							i++;
						}
					}
				}
			}
			areaIndex = areaIndecesCopy[Random.Range(0, areaArray.Length - numDestroyed)];
		}
		return areaIndex;
	}
}
