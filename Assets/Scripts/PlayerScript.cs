﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {	
	public Rigidbody2D player;
	public GameObject energyBar;
	public GameObject gameManager;
	public float speed;

	public float maxEnergy = 100f;
	public float currentEnergy;
	private bool isCharging = false;
	private GameScript gameScript;

	void Start() {
		player = GetComponent<Rigidbody2D>();
		currentEnergy = maxEnergy;
		gameScript = gameManager.GetComponent<GameScript>();
	}

	void Update() {
		if (Input.GetKey(KeyCode.LeftArrow)) {
			if (Input.GetKey (KeyCode.LeftShift)) {
				player.velocity = new Vector2 (-speed / 2, 0);
			} 
			else {
				player.velocity = new Vector2 (-speed, 0);
			}
		}
		else if (Input.GetKey(KeyCode.RightArrow)) {
			if (Input.GetKey (KeyCode.LeftShift)) {
				player.velocity = new Vector2 (speed / 2, 0);
			} 
			else {
				player.velocity = new Vector2 (speed, 0);
			}
		}
		else if (Input.GetKey(KeyCode.UpArrow)) {
			if (Input.GetKey (KeyCode.LeftShift)) {
				player.velocity = new Vector2 (0, speed / 2);
			} 
			else {
				player.velocity = new Vector2 (0, speed);
			}
		}
		else if (Input.GetKey(KeyCode.DownArrow)) {
			if (Input.GetKey (KeyCode.LeftShift)) {
				player.velocity = new Vector2 (0, -speed / 2);
			} 
			else {
				player.velocity = new Vector2 (0, -speed);
			}
		}
		else {
			player.velocity = new Vector2(0, 0);
		}
			
		if (isCharging) {
			if (currentEnergy > 0) {
				currentEnergy -= 1/(float)gameScript.numAreasAlive;
				SetEnergyBar(currentEnergy / maxEnergy);
			}
		}
	}
		
	void OnCollisionEnter2D(Collision2D obj) {
		if (obj.gameObject.tag == "Battery" && !isCharging) {
			BatteryScript bt = obj.gameObject.GetComponent<BatteryScript>();
			if (currentEnergy > 0) {
				isCharging = true;
				bt.charge++;
			}
		}
	}

	void OnCollisionStay2D(Collision2D obj) {
		if (obj.gameObject.tag == "Battery" && isCharging) {
			BatteryScript bt = obj.gameObject.GetComponent<BatteryScript>();
			if (currentEnergy <= 0) {
				isCharging = false;
				bt.charge = 0;
			}
		}
	}

	void OnCollisionExit2D(Collision2D obj) {
		if (isCharging) {
			if (obj.gameObject.tag == "Battery") {
				isCharging = false;
				BatteryScript bt = obj.gameObject.GetComponent<BatteryScript>();
				bt.charge = 0;
			}
		}
	}

	public void SetEnergyBar(float energy) {
		energyBar.transform.localScale = new Vector2(
			Mathf.Clamp(energy, 0f, 1f),
			energyBar.transform.localScale.y
		);
	}
}
