﻿using UnityEngine;
using System.Collections;

public class PowerStationScript : MonoBehaviour {
	public float maxTime, cooldownDecrement;
	public GameObject readyIndicator;
	public GameObject stationTop, stationBottom;

    private float currentTime;
	private bool used = false;
	private Color dimColor;

	void Start() {
		currentTime = maxTime;
		dimColor.r = 255;
		dimColor.g = 255;
		dimColor.b = 0;
		dimColor.a = .5f;
    }
	
	void Update () {
		if (used) {
			currentTime -= Time.deltaTime;
			if (currentTime <= 0) {
				used = false;
				if (maxTime > 1f) {
					maxTime -= cooldownDecrement;
				}
				currentTime = maxTime;
                readyIndicator.GetComponent<SpriteRenderer>().color = Color.green;
                stationTop.GetComponent<SpriteRenderer>().color = Color.yellow;
                stationBottom.GetComponent<SpriteRenderer>().color = Color.yellow;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (!used && collider.CompareTag("Player")) {
			AudioSource audio = GetComponent<AudioSource>();
			PlayerScript plScript = collider.gameObject.GetComponent<PlayerScript>();
			plScript.currentEnergy = plScript.maxEnergy;
			plScript.SetEnergyBar(plScript.currentEnergy / plScript.maxEnergy);

			used = true;
            readyIndicator.GetComponent<SpriteRenderer>().color = Color.red;
            stationTop.GetComponent<SpriteRenderer>().color = dimColor;
            stationBottom.GetComponent<SpriteRenderer>().color = dimColor;

			audio.Play();
		}
	}
}
