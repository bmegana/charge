﻿using UnityEngine;
using System.Collections;

public class NPCScript : MonoBehaviour {

	public GameObject npc;
    private bool isDamaging = false;

	void OnCollisionEnter2D(Collision2D obj) {
		if (obj.gameObject.tag == "Resource" && !isDamaging) {
			BatteryScript bt = obj.gameObject.GetComponentInChildren<BatteryScript>();
			bt.dmg += 1.0625f;
            isDamaging = true;
		}
	}

	void OnCollisionExit2D(Collision2D obj) {
		if (obj.gameObject.tag == "Resource" && isDamaging) {
			BatteryScript bt = obj.gameObject.GetComponentInChildren<BatteryScript>();
			bt.dmg -= 1.0625f;
            isDamaging = false;
		}
	}
}
