﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameScript : MonoBehaviour {
	public int numAreasAlive;
	public Text screenText;

	AudioSource[] sounds;
	private float time;
	private float guiTimer;

	private bool blankSet = false;
	private bool gameOverPlayed = false;

	void Start () {
		GameObject areaManager = GameObject.Find("Areas");
		AreaManagerScript areaManScript = areaManager.GetComponent<AreaManagerScript>();
		numAreasAlive = areaManScript.areas.Length;
		time = Time.time;

		sounds = GetComponents<AudioSource>();
		sounds[0].Play();

		guiTimer = 17.5f;
		SetGameplayText();
	}
	
	void Update () {
		if (guiTimer > 0) {
			guiTimer -= Time.deltaTime;
		} 
		else {
			if (!blankSet) {
				screenText.text = "";
				blankSet = true;
			}
		}

		if (numAreasAlive <= 0) {
			time = Time.timeSinceLevelLoad;
			sounds[0].Stop();
			if (!gameOverPlayed) {
				SetGameOverGUIText();
				sounds[1].Play();
				gameOverPlayed = true;
			}
			if (Input.GetKeyDown(KeyCode.Space)) {
				time = Time.timeSinceLevelLoad;
				SceneManager.LoadScene(0);
			}
			if (Input.GetKeyDown (KeyCode.Q)) {
				Application.Quit();
			}
		}
	}

	void SetGameplayText() {
		screenText.text = "Charge!!!!!!!!\n" +
		 "Arrow Keys to Move\n LShift Slows\n Touch the yellow boxes!\n" +
		 "Watch the center!";
		screenText.color = Color.yellow;
	}

	void SetGameOverGUIText() {
		screenText.text = "Game Over\n You lasted " + 
		 (int)time + " seconds\n Press Space to Restart\n Press Q to Exit";
		screenText.color = new Color(.6f, 0f, 0f);
	}
}
