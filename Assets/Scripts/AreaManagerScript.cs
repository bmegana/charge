﻿using UnityEngine;
using System.Collections;

public class AreaManagerScript : MonoBehaviour {
	public GameObject[] areas;

	// Use this for initialization
	void Start () {
		if (areas == null) {
			areas = GameObject.FindGameObjectsWithTag("Resource");
		}
	}
}
