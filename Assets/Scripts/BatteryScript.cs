﻿using UnityEngine;
using System.Collections;

public class BatteryScript : MonoBehaviour {
	public GameObject healthBar;
	public GameObject area;
	public float dmg = 0;
	public float charge = 0;

	public GameObject gameManager;
	private GameScript gameScript;

	public GameObject player;
	private PlayerScript plScript;

	private float maxHealth = 100f;
	private float currentHealth;

	void Start() {
		currentHealth = maxHealth;
		gameScript = gameManager.GetComponent<GameScript>();
		plScript = player.GetComponent<PlayerScript>();
	}

	void FixedUpdate () {
		if (dmg > 0) {
			Damage();
		}
		if (charge > 0) {
			if (plScript.currentEnergy <= 0) {
				charge = 0;
			}
			Recharge();
		}
	}

	public void Damage() {
		if (currentHealth > 0) {
			currentHealth -= dmg / 30;
			SetHealthBar(currentHealth / maxHealth);
		}

		if (currentHealth <= 0) {
			gameScript.numAreasAlive--;
			area.SetActive(false);
		}
	}

	public void Recharge() {
		if (currentHealth < maxHealth) {
			currentHealth += charge;
			SetHealthBar(currentHealth / maxHealth);
		}
	}

	void SetHealthBar(float health) {
		healthBar.transform.localScale = new Vector2(
			Mathf.Clamp(health, 0f, 1f),
			healthBar.transform.localScale.y
		);
	}
}
