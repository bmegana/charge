﻿using UnityEngine;
using System.Collections;

public class NPCManagerScript : MonoBehaviour {
	public int spawnNumber;
	public float maxSpawnTime;
	public GameObject npc;
	public Vector3 spawnPoint;
	private float currentSpawnTime;

	void Start() {
		currentSpawnTime = maxSpawnTime;
	}

	// Update is called once per frame
	void Update () {
		currentSpawnTime--;
		if (currentSpawnTime <= 0) {
			maxSpawnTime *= 2;
			currentSpawnTime = maxSpawnTime;
			for (int i = 0; i < spawnNumber; i++) {
				Instantiate(npc, spawnPoint, Quaternion.identity);
			}
		}
	}
}
